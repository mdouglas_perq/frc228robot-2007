/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: drive.h
 * Purpose: Drive control settings/declarations.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 14 2007  Matt           Settings/Function prototypes added.
 * May 16 2007  Matt           Added Documentation.                                           
 *
 *************************************************************************/

#ifndef _DRIVE_H_
#define _DRIVE_H_

/* Comment the definition of DRIVE_DEBUG to disable
   debugging outputs related to the arm. */
//#define DRIVE_DEBUG

/*todo: rename to DRIVE_LEFT_MOTOR/DRIVE_RIGHT_MOTOR*/

/* Set this to the PWM output port where the left
   drive is connected. */
#define LEFT_DRIVE_MOTOR 1

/* Set this to the PWM output port where the right
   drive is connected. */
#define RIGHT_DRIVE_MOTOR 2

/* Uncomment LEFT_DRIVE_INVERT or RIGHT_DRIVE_INVERT
   to invert all output sent to the left and right
   drives through SetLeftDrive() and SetRightDrive(),
   respectively. These should be set up so that
   values > 127 moves the drive in a forward direction,
   and that values < 127 moves the drive in reverse. */
#define LEFT_DRIVE_INVERT
/*#define RIGHT_DRIVE_INVERT*/

/* Set this to the OI joystick port used to control
   the left drive with. */
#define LEFT_DRIVE_JOYSTICK PORT_1

/* Set this to the axis on the joystick used to
   control the left drive with. */
#define LEFT_DRIVE_JOYSTICK_AXIS Y_AXIS

/* Set this to the OI joystick port used to control
   the right drive with. */
#define RIGHT_DRIVE_JOYSTICK PORT_2

/* Set this to the axis on the joystick used to
   control the right drive with. */
#define RIGHT_DRIVE_JOYSTICK_AXIS Y_AXIS

/* Set this to the Digital I/O port where the
   drive's left side encoder is connected. */
#define LEFT_DRIVE_ENCODER 1

/* Set this to the Digital I/O port where the
   drive's right side encoder is connected. */
#define RIGHT_DRIVE_ENCODER 2

/* Set this to the Analog I/O port where the
   drive base's gyro is connected. */
#define DRIVE_GYRO 1

/* Set this to the type of gyro used on the drive.
   Default: ADXRS300 */
#define DRIVE_GYRO_TYPE ADXRS300

/* Gyro dedband setting. Todo: document this. */
#define DRIVE_GYRO_DEADBAND 3

/* Set this to the Analog I/O port where the
   drive base's accelerometer's x-axis is connected. */
#define DRIVE_ACCELEROMETER_X 2

/* Set this to the Analog I/O port where the
   drive base's accelerometer's y-axis is connected. */
#define DRIVE_ACCELEROMETER_Y 3


/*
 * FUNCTION PROTOTYPES
 */
void SetLeftDrive(unsigned char value);
void SetRightDrive(unsigned char value);
int GetDriveHeading(void);
int GetDriveHeadingWithRollover(void);
void ControlDrive(void);

#endif  /* #ifndef _DRIVE_H_ */
