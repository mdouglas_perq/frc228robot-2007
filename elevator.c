/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: elevator.c
 * Purpose: Elevator control.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 14 2007  Matt           Added SetElevatorMotor() and ArmPID().
 * May 16 2007  Matt           Added ControlElevator(), Debugging, Docs.
 * May 16 2007  Matt           Tested, fixed elevator control bug..
 *                                            
 *
 *************************************************************************/

#include "gus.h"
#include "elevator.h"

/**************************************************************************
 *    Function: SetElevatorMotor
 *     Purpose: Control the output to the right side of the drive train.
 *   Arguments: unsigned char value
 *                  0 < value < 254; 
 *                  value > 127 => Down
 *                  value = 127 => Stop
 *                  value < 127 => Up
 *     Returns: void
 * Called From: ControlElevator(), ElevatorPID(), Anywhere in general.
 *       Notes:
 *************************************************************************/
void SetElevatorMotor(unsigned char value)
{

#ifdef ELEVATOR_DEBUG
    PrintToScreen("[elevator.c] SetElevatorMotor(%d)\r", (int)value);
#endif

    /* deadband 114 - 140 */
    if (value > 114 && value < 140)
        value = 127;

    /* Limit downward speed */
    if (value > 142)
        value = 142;

    //if (value < 112)
       // value = 112;

   // if (value > 135)
  //      value = 135;

#ifdef ELEVATOR_MOTOR_INVERT
    SetPWM(ELEVATOR_MOTOR, 254 - value);
#else
    SetPWM(ELEVATOR_MOTOR, value);
#endif

}   /* void SetElevatorMotor(unsigned char value) */


/**************************************************************************
 *    Function: ElevatorPID
 *     Purpose: Move the elevator to a target position using a PID control loop.
 *   Arguments: int target
 *                 The desired elevator position, based on the IR sensor.
 *                 ARM_TARGET_BOTTOM <= target < ARM_TARGET_TOP
 *     Returns: An integer representing the state of the control loop.
 *              ELEVATOR_PID_RUNNING will be returned if the arm is not on
 *                target and is still attempting to reach it.
 *              ELEVATOR_PID_ONTARGET will be returned if the arm has reached
 *                its target position.
 * Called From: ControlElevator(), Anywhere in general.
 *       Notes: This is meant to be called iteratively in a loop. Do not
 *              call this function just once.
 *************************************************************************/
int ElevatorPID(int target)
{
    int error;                              /* Current error from target. */
    int current;                            /* Current sensor reading. */
    int status = ELEVATOR_PID_RUNNING;      /* Return status code. */

    /* Calculate the current error from the target position. */
    current = GetAnalogInput(ELEVATOR_IR_SENSOR);
    error = current - target;

#ifdef ELEVATOR_DEBUG
    PrintToScreen("\r[elevator.c] ----- ElevatorPID Debugging -----\r");
    PrintToScreen("[elevator.c]  Elevator Target: %d\r", target);
    PrintToScreen("[elevator.c] Elevator Current: %d\r", current);
    PrintToScreen("[elevator.c]   Elevator Error: %d\r", error);
#endif

    /* Are we close enough, or are we off? */
    if ((error > ELEVATOR_ERROR_THRESHOLD) || (error < (-1 * ELEVATOR_ERROR_THRESHOLD)))
    {
        /* Not on target. */

        int delta_error;                    /* The change in error since the last check. */
        int p_out, i_out, d_out;            /* Outputs for P, I, and D controls. */
        int output;                         /* Overall output. */

        static int integral_error = 0;      /* Integral error. (Sum of error over time) */
        static int prev_error = 0;          /* Last recorded error. */

        /* Calculate the change in error. */
        delta_error = prev_error - error;
    
        /* TODO: Only integrate error when error has changed since last reading. */
        integral_error += error;

        /* Prevent integral windup with limited values */
        if (integral_error > 200)
            integral_error = 200;
        else if (integral_error < -200)
            integral_error = -200;

        /* Calculate output. */
        p_out = error * ELEVATOR_PID_KP;
        i_out = integral_error * ELEVATOR_PID_KI;
        d_out = delta_error * ELEVATOR_PID_KD;

        output = p_out + i_out + d_out;

        /* Limit output. At this point, output will be negative
           if we need to move down, and positive if we need to
           move to up.
        */
        if (output > 127)
            output = 127;
        else if (output < -127)
            output = -127;

#ifdef ELEVATOR_DEBUG
        PrintToScreen("[elevator.c]  Elevator Status: Not on Target\r");
        PrintToScreen("[elevator.c] Elevator IntgErr: %d\r", integral_error);
        PrintToScreen("[elevator.c] Elevator DeltErr: %d\r", delta_error);
        PrintToScreen("[elevator.c]  Elevator Output: %d [Signed]\r", output);
#endif

        /* Send the output to the motor. */
        output += 127;
        SetElevatorMotor((unsigned char)255-output);
        
        status = ELEVATOR_PID_RUNNING;

#ifdef ELEVATOR_DEBUG
        PrintToScreen("[elevator.c]  Elevator Output: %d [Unsigned]\r", output);
#endif

    }   /* if ((error > ELEVATOR_ERROR_THRESHOLD) || (error < (-1 * ELEVATOR_ERROR_THRESHOLD))) */
    else
    {
        /* We're on target. Don't move. */

#ifdef ELEVATOR_DEBUG
        PrintToScreen("[elevator.c]  Elevator Status: On Target\r");
        PrintToScreen("[elevator.c]  Elevator Output: 127 [Unsigned]\r");
#endif

       /* SetElevatorMotor(127); */
        SetElevatorMotor(110);      /* prevent backdriving */
        status = ELEVATOR_PID_ONTARGET;
    }

#ifdef ELEVATOR_DEBUG
    PrintToScreen("[elevator.c] --- End ElevatorPID Debugging ---\r");
#endif

    return status;

}   /* int ElevatorPID(int target) */

/**************************************************************************
 *    Function: ControlElevator
 *     Purpose: Controls the elevator via Operator Interface inputs.
 *   Arguments: void
 *     Returns: void
 * Called From: operator.c/OperatorControl()
 *       Notes: This is meant to be called iteratively in a loop;
 *              specifically only in operator.c/OperatorControl()
 *************************************************************************/
void ControlElevator(void)
{
    unsigned char bottom_button, low_button, middle_button, top_button, joystick;
    unsigned int elevator_sensor;

    /* Get button states. */
    bottom_button = GetOIDInput(ELEVATOR_JOYSTICK, ELEVATOR_BUTTON_BOTTOM);
    low_button = GetOIDInput(ELEVATOR_JOYSTICK, ELEVATOR_BUTTON_LOW);
    middle_button = GetOIDInput(ELEVATOR_JOYSTICK, ELEVATOR_BUTTON_MIDDLE);
    top_button = GetOIDInput(ELEVATOR_JOYSTICK, ELEVATOR_BUTTON_TOP);

    /* Get joystick input. */
    joystick = GetOIAInput(ELEVATOR_JOYSTICK, Y_AXIS);  /* todo: define ELEVATOR_JOYSTICK_AXIS */

    /* Get sensor input. */
    elevator_sensor = GetAnalogInput(ELEVATOR_IR_SENSOR);

    /* if (!bottom_button && !low_button && !middle_button && !top_button) */
    if (bottom_button == 0 && low_button == 0 && middle_button == 0 && top_button == 0)
    {
        /* Joystick control. */
        int output = joystick;
        if (joystick > 117 && joystick < 137)
            output = 140;


#ifdef ELEVATOR_DEBUG
        PrintToScreen("[elevator.c] ControlElevator(): Control = Joystick; Output = %d\r", (int)joystick);
#endif


       // SetElevatorMotor(255-joystick);
        SetElevatorMotor(255-output);

    }   /* if (bottom_button == 0 && low_button == 0 && middle_button == 0 && top_button == 0) */
    else
    {
        int target = elevator_sensor; 

        if (bottom_button == 1)
            target = ELEVATOR_TARGET_BOTTOM;
        else if (low_button == 1)
            target = ELEVATOR_TARGET_LOW;
        else if (middle_button == 1)
            target = ELEVATOR_TARGET_MIDDLE;
        else if (top_button == 1)
            target = ELEVATOR_TARGET_TOP;

        /*
        if (bottom_button)
            target = ELEVATOR_TARGET_BOTTOM;
        else if (low_button)
            target = ELEVATOR_TARGET_LOW;
        else if (middle_button)
            target = ELEVATOR_TARGET_MIDDLE;
        else if (top_button)
            target = ELEVATOR_TARGET_TOP;
        */

#ifdef ELEVATOR_DEBUG
        PrintToScreen("[elevator.c] ControlElevator(): Control Mode = Button/PID, Target = %d\r", target);
#endif

        ElevatorPID(target);
    }
    
}   /* void ControlElevator(void) */
