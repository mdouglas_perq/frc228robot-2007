/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: operator.c
 * Purpose: Operator control.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 *                                            
 *
 *************************************************************************/

#include "gus.h"
#include "arm.h"
#include "claw.h"
#include "drive.h"
#include "elbow.h"
#include "elevator.h"

void OperatorControl(void)
{
    StartGyro(ARM_GYRO);    /* TODO: StartArm() function? */

    while (1)
    {
        ControlArm();
        ControlClaw();
        ControlDrive();
        ControlElbow();
        ControlElevator();
    }
}
