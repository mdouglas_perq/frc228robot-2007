/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: main.c
 * Purpose: Robot initialization.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 11 2007  Matt           File created.
 *                                            
 *
 *************************************************************************/

#include "gus.h"
#include "arm.h"
#include "claw.h"
#include "drive.h"
#include "elbow.h"

/**************************************************************************
 *    Function: IO_Initialization
 *     Purpose: Sets up the competition mode.
 *   Arguments: void
 *     Returns: void
 * Called From: WPILib
 *       Notes: This should not need to be modified.
 *************************************************************************/
void IO_Initialization(void)
{

    /* Use full field controls. */
    SetCompetitionMode(1);

}   /* void IO_Initialization(void) */


/**************************************************************************
 *    Function: Initialize
 *     Purpose: Initialization: called when the robot is powered on.
 *   Arguments: void
 *     Returns: void
 * Called From: WPILib
 *       Notes: 
 *************************************************************************/
void Initialize(void)
{

#ifdef INIT_DEBUG
    PrintToScreen("\r\r");
    PrintToScreen("[main.c] ====== GUS Robotics Team 228 =====\r");
    PrintToScreen("[main.c]  2007 FRC Robot Control Software \r");
    PrintToScreen("[main.c] ==================================\r\r");
    PrintToScreen("[main.c] Initializing. Stand by...\r\r");

    PrintToScreen("[main.c] Initializing pressure switch... ");
#endif 

    InitPressureSwitch(PRESSURE_SWITCH_PORT, PRESSURE_SWITCH_RELAY);

#ifdef INIT_DEBUG
    PrintToScreen("OK\r[main.c] Initializing Arm gyro... ");
#endif

    /* Initialize the Arm's gyro.
       TODO: Make an InitArm() function?
    */
    SetGyroType(ARM_GYRO, ARM_GYRO_TYPE);
    SetGyroDeadband(ARM_GYRO, ARM_GYRO_DEADBAND);
    InitGyro(ARM_GYRO);

#ifdef INIT_DEBUG
    PrintToScreen("OK\r[main.c] Initializing Drive gyro... ");
#endif

    /* Initialize the Drive's gyro.
       TODO: Make an InitDrive() function?
    */
    SetGyroType(DRIVE_GYRO, DRIVE_GYRO_TYPE);
    SetGyroDeadband(DRIVE_GYRO, DRIVE_GYRO_TYPE);
    InitGyro(DRIVE_GYRO);

#ifdef INIT_DEBUG
    PrintToScreen("OK\r[main.c] Initializing Drive accelerometers... ");
#endif

    /* Initialize Drive accelerometers.
       TODO: InitDrive() function?
    */
    InitAccelerometer(DRIVE_ACCELEROMETER_X);
    InitAccelerometer(DRIVE_ACCELEROMETER_Y);

#ifdef INIT_DEBUG
    PrintToScreen("OK\r[main.c] Initializing Claw/Elbow states... ");
#endif

    /* Initialize Elbow/Claw states.
       TODO: InitElbow(), InitClaw() functions?
    */
    ElbowUp();
    CloseClaw();

#ifdef INIT_DEBUG
    PrintToScreen("OK\r\r[main.c] Initialization complete.\r\r");
#endif

}   /* void Initialize(void) */

/**************************************************************************
 *    Function: main
 *     Purpose: To satisfy linker requirements.
 *   Arguments: void
 *     Returns: void
 * Called From: N/A
 *       Notes: This should not need to be modified.
 *************************************************************************/
void main(void) { }
