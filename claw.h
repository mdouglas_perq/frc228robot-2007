/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: claw.h
 * Purpose: Claw control settings/declarations.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 16 2007  Matt           Added Documentation.                                          
 *
 *************************************************************************/

#ifndef _CLAW_H_
#define _CLAW_H_

/* Comment the definition of CLAW_DEBUG to disable
   debugging outputs related to the claw. */
#define CLAW_DEBUG

/* Set this to the relay port where the claw's spike is connected. */
#define CLAW_RELAY 1

/* Return values for GetClawState(). No need to change. */
#define CLAW_STATE_OPEN 0
#define CLAW_STATE_CLOSED 1

/* Set this to the joystick port where the OI's claw
   control button is connected. */
#define CLAW_BUTTON_JOYSTICK PORT_2

/* Set this to the switch that the OI's claw button
   is using. */
#define CLAW_BUTTON TOP_SW

/*
 * FUNCTION PROTOTYPES
 */
void OpenClaw(void);
void CloseClaw(void);
int GetClawState(void);
void ToggleClawState(void);
void ControlClaw(void);

#endif  /* #ifndef _CLAW_H_ */
