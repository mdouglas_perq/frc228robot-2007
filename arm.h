/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: arm.h
 * Purpose: Arm control settings/declarations.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 14 2007  Matt           Added gyro settings.   
 * May 16 2007  Matt           Added documentation.                                        
 *
 *************************************************************************/

#ifndef _ARM_H_
#define _ARM_H_

/* Comment the definition of ARM_DEBUG to disable
   debugging outputs related to the arm. */
#define ARM_DEBUG

/* Set this to the PWM output where the arm's
   motor is connected. */
#define ARM_MOTOR 4

/* Uncomment the definition of ARM_MOTOR_INVERT to
   invert output to the arm motor. This should be
   set so that values > 127 cause the arm to move
   to the right, and values < 127 cause the arm
   to move to the left, when calling SetArmMotor(). */
/*#define ARM_MOTOR_INVERT*/


/** ARM GYRO SETTINGS **/

/* Set this to the Analog I/O port where the
   arm's gyro is connected. */
#define ARM_GYRO 5

/* Set this to the type of gyro used by the arm.
   Default: ADXRS150 */
#define ARM_GYRO_TYPE ADXRS150

/* Gyro deadband setting. Todo: document this. */
#define ARM_GYRO_DEADBAND 5

/** END ARM GYRO SETTINGS **/


/** ARM PID CONTROL SETTINGS **/

/* Return values for ArmPID(). Using words makes it much clearer. 
   These should not need to be changed. */
#define ARM_PID_RUNNING 0
#define ARM_PID_ONTARGET 1

/* PID control coefficients. */
#define ARM_PID_KP 14 / 10
#define ARM_PID_KI 0 / 10
#define ARM_PID_KD 0 / 10

/* Target gyro values for the three standard positions. */
#define ARM_TARGET_LEFT -85
#define ARM_TARGET_CENTER 5 //0
#define ARM_TARGET_RIGHT 80

/* Set this to the amount of acceptable error from the
   target value. ArmPID() will report the arm as being
   on target if the current reading is within 
   +/-ARM_ERROR_THRESHOLD. */
#define ARM_ERROR_THRESHOLD 5 /*5*/

/** END ARM PID CONTROL SETTINGS **/

/* Set this to the joystick port where the OI's arm control 
   potentiometer is connected. */
#define ARM_POT PORT_4

/* Set this to the axis used by the OI's arm control pot. */
#define ARM_POT_AXIS X_AXIS 


/*
 * FUNCTION PROTOTYPES
 */
void SetArmMotor(unsigned char value);
int GetArmHeading(void);
int ArmPID(int target);
void ControlArm(void);

#endif  /* #ifndef _ARM_H_ */
