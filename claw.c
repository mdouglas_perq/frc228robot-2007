/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: claw.c
 * Purpose: Claw control.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 16 2007  Matt           Added Debugging, TODO note.
 *                                            
 *
 *************************************************************************/

#include "gus.h"
#include "claw.h"

static int claw_state = CLAW_STATE_OPEN;

/*
  TODO: Define settings CLAW_OPEN_RELAY_FWD, CLAW_OPEN_RELAY_REV and/or
                        CLAW_CLOSED_RELAY_FWD, CLAW_CLOSED_RELAY_REV that
                        define which direction the relays should go to 
                        open/close the claw.
*/

/**************************************************************************
 *    Function: OpenClaw
 *     Purpose: Opens the claw.
 *   Arguments: void
 *     Returns: void
 * Called From: ToggleClawState(), Anywhere in general.
 *       Notes: 
 *************************************************************************/
void OpenClaw(void)
{

#ifdef CLAW_DEBUG
    PrintToScreen("[claw.c] OpenClaw()\r");
#endif

    SetRelay(CLAW_RELAY, 0, 1);         /* Relay Rev */

    claw_state = CLAW_STATE_OPEN;

}   /* void OpenClaw(void) */

/**************************************************************************
 *    Function: CloseClaw
 *     Purpose: Closes the claw.
 *   Arguments: void
 *     Returns: void
 * Called From: ToggleClawState(), Anywhere in general.
 *       Notes:
 *************************************************************************/
void CloseClaw(void)
{

#ifdef CLAW_DEBUG
    PrintToScreen("[claw.c] CloseClaw()\r");
#endif

    SetRelay(CLAW_RELAY, 1, 0);     /* Relay Fwd */

    claw_state = CLAW_STATE_CLOSED;

}   /* void CloseClaw(void) */

/**************************************************************************
 *    Function: GetClawState
 *     Purpose: Accessor to retreive the claw's current state.
 *   Arguments: void
 *     Returns: An integer, either CLAW_STATE_OPEN or CLAW_STATE_CLOSED.
 * Called From: ToggleClawState(), Anywhere in general.
 *       Notes:
 *************************************************************************/
int GetClawState(void)
{

#ifdef CLAW_DEBUG
    PrintToScreen("[claw.c] GetClawState(): returning %s\r", 
                     (claw_state == CLAW_STATE_OPEN) ?
                           "CLAW_STATE_OPEN" : "CLAW_STATE_CLOSED");
#endif

    return claw_state;

}   /* int GetClawState(void) */

/**************************************************************************
 *    Function: ToggleClawState
 *     Purpose: Inverts the current state of the claw.
 *   Arguments: void
 *     Returns: void
 * Called From: ControlClaw(), Anywhere in general.
 *       Notes:
 *************************************************************************/
void ToggleClawState(void)
{
    if (GetClawState() == CLAW_STATE_CLOSED)
        OpenClaw();
    else
        CloseClaw();

}   /* void ToggleClawState(void) */

/**************************************************************************
 *    Function: ControlClaw
 *     Purpose: Controls the claw via Operator Interface inputs.
 *   Arguments: void
 *     Returns: void
 * Called From: operator.c/OperatorControl()
 *       Notes: This is meant to be called iteratively in a loop;
 *              specifically only in operator.c/OperatorControl()
 *************************************************************************/
void ControlClaw(void)
{
    static int counter = 0;
    unsigned char claw_button = GetOIDInput(CLAW_BUTTON_JOYSTICK, CLAW_BUTTON);
    
    counter++;

    if (claw_button == 1 && counter > 40)
    {

#ifdef CLAW_DEBUG
        PrintToScreen("[claw.c] ControlClaw() : Button latched. Calling ToggleClawState()\r");
#endif
        ToggleClawState();
        counter = 0;
    }

}   /* void ControlClaw(void) */
    
