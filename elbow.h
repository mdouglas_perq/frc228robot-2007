/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: claw.h
 * Purpose: Claw control settings/declarations.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 14 2007  Matt           Settings/Function prototypes added.
 * May 16 2007  Matt           Added Documentation.                                            
 *
 *************************************************************************/

#ifndef _ELBOW_H_
#define _ELBOW_H_

/* Comment the definition of ELBOW_DEBUG to disable
   debugging outputs related to the claw. */
#define ELBOW_DEBUG

/* Set this to the relay port where the elbow's spike is connected. */
#define ELBOW_RELAY 7

/* Return values for GetElbowState(). No need to change. */
#define ELBOW_STATE_UP 0
#define ELBOW_STATE_DOWN 1

/* Set this to the joystick port where the OI's elbow
   control button is connected. */
#define ELBOW_BUTTON_JOYSTICK PORT_2

/* Set this to the switch that the OI's claw button
   is using. */
#define ELBOW_BUTTON TRIGGER_SW

/*
 * FUNCTION PROTOTYPES
 */
void ElbowUp(void);
void ElbowDown(void);
int GetElbowState(void);
void ToggleElbowState(void);
void ControlElbow(void);

#endif  /* #ifndef _ELBOW_H_ */
