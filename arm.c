/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: arm.c
 * Purpose: Arm control.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 11 2007  Matt           File created.
 * May 14 2007  Matt           Added documentation.                                          
 *
 *************************************************************************/

#include "gus.h"
#include "arm.h"
#include "claw.h"
#include "elbow.h"
#include "elevator.h"

/**************************************************************************
 *    Function: SetArmMotor
 *     Purpose: Control the output to the arm's motor.
 *   Arguments: unsigned char value
 *                  0 < value < 254; 
 *                  value > 127 => Move Right
 *                  value = 127 => Stop
 *                  value < 127 => Move Left
 *     Returns: void
 * Called From: ArmPID(), Anywhere in general.
 *       Notes:
 *************************************************************************/
void SetArmMotor(unsigned char value)
{

#ifdef ARM_DEBUG
    PrintToScreen("[arm.c] SetArmMotor(%d)\r", (int)value);
#endif

#ifdef ARM_MOTOR_INVERT
    SetPWM(ARM_MOTOR, 254 - value);
#else
    SetPWM(ARM_MOTOR, value);
#endif

}   /* void SetArmMotor(unsigned char value) */

/**************************************************************************
 *    Function: GetArmHeading
 *     Purpose: Access the heading read by the arm's gyro.
 *   Arguments: void
 *     Returns: A signed integer represeting the orientation, in degrees.
 * Called From: ArmPID(), Anywhere in general.
 *       Notes:
 *************************************************************************/
int GetArmHeading(void)
{
    return (GetGyroAngle(ARM_GYRO) / 10);

}   /* int GetArmHeading(void) */


/**************************************************************************
 *    Function: ArmPID
 *     Purpose: Move the arm to a target position using a PID control loop.
 *   Arguments: int target
 *                 The desired arm position, in degrees, from the center.
 *                 ARM_TARGET_LEFT < target < ARM_TARGET_RIGHT
 *     Returns: An integer representing the state of the control loop.
 *              ARM_PID_RUNNING will be returned if the arm is not on
 *                target and is still attempting to reach it.
 *              ARM_PID_ONTARGET will be returned if the arm has reached
 *                its target position.
 * Called From: ControlArm(), Anywhere in general.
 *       Notes: This is meant to be called iteratively in a loop. Do not
 *              call this function just once.
 *************************************************************************/
int ArmPID(int target)
{
    int error;                          /* Current error from target */
    int heading;                        /* Arm's position */
    int status = ARM_PID_RUNNING;       /* Return status code. */
    
    /* Calculate the current error from the target position. */
    heading = GetArmHeading();
    error = target - heading;

#ifdef ARM_DEBUG
    PrintToScreen("\r[arm.c] ----- ArmPID Debugging -----\r");
    PrintToScreen("[arm.c]  Arm Target: %d\r", target);
    PrintToScreen("[arm.c] Arm Heading: %d\r", heading);
    PrintToScreen("[arm.c]   Arm Error: %d\r", error);
#endif

    /* Are we close enough, or are we off? */
    if ((error > ARM_ERROR_THRESHOLD) || (error < (-1 * ARM_ERROR_THRESHOLD)))
    {
        /* Not on target. */

        int delta_error;                    /* The change in error since the last check. */
        int p_out, i_out, d_out;            /* Outputs for P, I, and D controls */
        int output;                         /* Overall output */

        static int integral_error = 0;      /* Integral error (sum of error over time) */
        static int prev_error = 0;          /* Last recorded error */

        /* Calculate the change in error. */
        delta_error = error - prev_error;
        
        /* TODO: Only integrate error when error has changed since last reading. */

        /* Integrate the error. */
        integral_error += error;

        /* Prevent integral windup with limited values */
        if (integral_error > 200)
            integral_error = 200;
        else if (integral_error < -200)
            integral_error = -200;

        /* Calculate output. */
        p_out = error * ARM_PID_KP;
        i_out = integral_error * ARM_PID_KI;
        d_out = delta_error * ARM_PID_KD;

        output = p_out + i_out + d_out;
        
        /* Limit output. At this point, output will be negative
           if we need to move left, and positive if we need to
           move to the right.
        */
        if (output > 127)
            output = 127;
        else if (output < -127)
            output = -127;

#ifdef ARM_DEBUG
        PrintToScreen("[arm.c]  Arm Status: Not on Target\r");
        PrintToScreen("[arm.c] Arm IntgErr: %d\r", integral_error);
        PrintToScreen("[arm.c] Arm DeltErr: %d\r", delta_error);
        PrintToScreen("[arm.c]  Arm Output: %d [Signed]\r", output);
#endif

        /* Send the output to the motor. */
        output += 127;
        SetArmMotor((unsigned char)output);

        status = ARM_PID_RUNNING;

#ifdef ARM_DEBUG
        PrintToScreen("[arm.c]  Arm Output: %d [Unsigned]\r", output);
#endif

    }   /* if ((error > ARM_ERROR_THRESHOLD) || (error < (-1 * ARM_ERROR_THRESHOLD))) */
    else
    {
        /* We're on target. Don't move. */

#ifdef ARM_DEBUG
        PrintToScreen("[arm.c]  Arm Status: On Target\r");
        PrintToScreen("[arm.c]  Arm Output: 127 [Unsigned]\r");
#endif

        SetArmMotor(114);
        status = ARM_PID_ONTARGET;
    }

#ifdef ARM_DEBUG
    PrintToScreen("[arm.c] --- End ArmPID Debugging ---\r");
#endif

    /* Return a status indication code. */
    return status;

}   /* int ArmPID(int target) */


/**************************************************************************
 *    Function: ControlArm
 *     Purpose: Controls the arm via Operator Interface inputs.
 *   Arguments: void
 *     Returns: void
 * Called From: operator.c/OperatorControl()
 *       Notes: This is meant to be called iteratively in a loop;
 *              specifically only in operator.c/OperatorControl()
 *************************************************************************/
void ControlArm(void)
{
    unsigned char arm_pot;
    int arm_target = GetArmHeading();

    arm_pot = GetOIAInput(ARM_POT, ARM_POT_AXIS);

#ifdef ARM_DEBUG
    PrintToScreen("[arm.c] ControlArm(): arm_pot = %d\r", (int)arm_pot);
#endif

    if (arm_pot > 117 && arm_pot < 137)     /* todo: define ARM_POT_DEADBANDMIN and ARM_POT_DEADBANDMAX */
    { 
        arm_target = ARM_TARGET_CENTER;
    }
    else if (arm_pot >= 137)                /* Move right. todo: use arm_pot >= ARM_POT_DEADBANDMAX */
    {
       /* arm_target = (((arm_pot - 127) * ARM_TARGET_RIGHT) / 127); */
        int delta = arm_pot - 127;
        //target = delta * (ARM_TARGET_RIGHT / 127);
        arm_target = (delta * ARM_TARGET_RIGHT) / 127;
    }
    else if (arm_pot <= 117)                /* Move left. todo: use arm_pot <= ARM_POT_DEADBANDMIN, optimize. */
    {
        /*arm_target = (((127 - arm_pot) * ARM_TARGET_LEFT) / 127); */
         int delta = 127 - arm_pot;
        //target = delta * (ARM_TARGET_LEFT / 127);
        arm_target = (delta * ARM_TARGET_LEFT) / 127;
    }

    /* Safety check. Close the claw if we're attempting to move the arm with the elbow up. */
    if (arm_target != ARM_TARGET_CENTER && GetElbowState() == ELBOW_STATE_UP && GetClawState() == CLAW_STATE_OPEN)
    {
        /*CloseClaw(); Forget this for now. */
    }

    /* Todo: Safety check for moving the arm with the elevator too low. */

    /* Move the arm to the desired position. */
    ArmPID(arm_target);

}   /* void ControlArm(void) */
