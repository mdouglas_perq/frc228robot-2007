/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: gus.h
 * Purpose: Main header file.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 *                                            
 *
 *************************************************************************/

#ifndef _GUS_H_
#define _GUS_H_

#include "BuiltIns.h"

#define INIT_DEBUG

/* Debug Macro. To be used as such:

   DEBUG_DELAY(500)
   {
       PrintToScreen("This is printed a maximum of once every 500ms\r");
   }

   within a function.

   todo: document this more.
*/
#define DEBUG_DELAY(x) if ((GetMsClock() % (x)) == 0)

#define PRESSURE_SWITCH_PORT 18
#define PRESSURE_SWITCH_RELAY 8


#endif  /* #ifndef _GUS_H_ */
