/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: elbow.c
 * Purpose: Elbow control.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 14 2007  Matt           Code added.
 * May 16 2007  Matt           Added Debugging, TODO note.
 *                                            
 *
 *************************************************************************/

#include "gus.h"
#include "elbow.h"

static int elbow_state = ELBOW_STATE_UP;

/*
  TODO: Define settings ELBOW_UP_RELAY_FWD, ELBOW_UP_RELAY_REV and/or
                        ELBOW_DOWN_RELAY_FWD, ELBOW_DOWN_RELAY_REV that
                        define which direction the relays should go to 
                        raise/lower the elbow.
*/

/**************************************************************************
 *    Function: ElbowUp
 *     Purpose: Raises the elbow.
 *   Arguments: void
 *     Returns: void
 * Called From: ToggleElbowState(), Anywhere in general.
 *       Notes: 
 *************************************************************************/
void ElbowUp(void)
{

#ifdef ELBOW_DEBUG
    PrintToScreen("[elbow.c] ElbowUp()\r");
#endif

    /*todo: figure out which is correct.*/
    SetRelay(ELBOW_RELAY, 1, 0);        /* Relay Fwd */
    /*SetRelay(ELBOW_RELAY, 0, 1);        /* Relay Rev */
    
    elbow_state = ELBOW_STATE_UP;

}   /* void ElbowUp(void) */

/**************************************************************************
 *    Function: ElbowDown
 *     Purpose: Folds the elbow.
 *   Arguments: void
 *     Returns: void
 * Called From: ToggleElbowState(), Anywhere in general.
 *       Notes: 
 *************************************************************************/
void ElbowDown(void)
{

#ifdef ELBOW_DEBUG
    PrintToScreen("[elbow.c] ElbowDown()\r");
#endif

    /*todo: figure out which is correct.*/
    SetRelay(ELBOW_RELAY, 0, 1);        /* Relay Rev */
    /*SetRelay(ELBOW_RELAY, 1, 0);        /* Relay Fwd */
    
    elbow_state = ELBOW_STATE_DOWN;

}   /* void ElbowDown(void) */

/**************************************************************************
 *    Function: GetElbowState
 *     Purpose: An accessor to retreive the elbow's current state.
 *   Arguments: void
 *     Returns: An integer, either ELBOW_STATE_UP or ELBOW_STATE_DOWN.
 * Called From: ToggleClawState(), Anywhere in general.
 *       Notes: 
 *************************************************************************/
int GetElbowState(void)
{
#ifdef CLAW_DEBUG
    PrintToScreen("[elbow.c] GetElbowState(): returning %s\r", 
                     (elbow_state == ELBOW_STATE_UP) ?
                           "ELBOW_STATE_UP" : "ELBOW_STATE_DOWN");
#endif

    return elbow_state;

}   /* int GetElbowState(void) */

/**************************************************************************
 *    Function: ToggleElbowState
 *     Purpose: Inverts the current state of the elbow.
 *   Arguments: void
 *     Returns: void
 * Called From: ControlElbow(), Anywhere in general.
 *       Notes: 
 *************************************************************************/
void ToggleElbowState(void)
{
    if (GetElbowState() == ELBOW_STATE_DOWN)
        ElbowUp();
    else
        ElbowDown();

}   /* void ToggleElbowState(void) */


/**************************************************************************
 *    Function: ControlElbow
 *     Purpose: Controls the elbow via Operator Interface inputs.
 *   Arguments: void
 *     Returns: void
 * Called From: operator.c/OperatorControl()
 *       Notes: This is meant to be called iteratively in a loop;
 *              specifically only in operator.c/OperatorControl()
 *************************************************************************/
void ControlElbow(void)
{
    static int counter = 0;
    unsigned char elbow_button = GetOIDInput(ELBOW_BUTTON_JOYSTICK, ELBOW_BUTTON);
    
    counter++;

    if (elbow_button == 1 && counter > 40)
    {

#ifdef CLAW_DEBUG
        PrintToScreen("[elbow.c] ControlElbow() : Button latched. Calling ToggleElbowState()\r");
#endif

        ToggleElbowState();
        counter = 0;
    }

}   /* void ControlElbow(void) */
