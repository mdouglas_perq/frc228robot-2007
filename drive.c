/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: drive.c
 * Purpose: Drive control.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 16 2007  Matt           Added Debugging.
 *                                            
 *
 *************************************************************************/

#include "gus.h"
#include "drive.h"

/**************************************************************************
 *    Function: SetLeftDrive
 *     Purpose: Control the output to the left side of the drive train.
 *   Arguments: unsigned char value
 *                  0 < value < 254; 
 *                  value > 127 => Forward
 *                  value = 127 => Stop
 *                  value < 127 => Reverse
 *     Returns: void
 * Called From: ControlDrive(), Anywhere in general.
 *       Notes:
 *************************************************************************/
void SetLeftDrive(unsigned char value)
{

#ifdef DRIVE_DEBUG
    PrintToScreen("[drive.c] SetLeftDrive(%d)\r", (int)value);
#endif

#ifdef LEFT_DRIVE_INVERT
    SetPWM(LEFT_DRIVE_MOTOR, 254 - value);
#else
    SetPWM(LEFT_DRIVE_MOTOR, value);
#endif

}   /* void SetLeftDrive(unsigned char value) */


/**************************************************************************
 *    Function: SetRightDrive
 *     Purpose: Control the output to the right side of the drive train.
 *   Arguments: unsigned char value
 *                  0 < value < 254; 
 *                  value > 127 => Forward
 *                  value = 127 => Stop
 *                  value < 127 => Reverse
 *     Returns: void
 * Called From: ControlDrive(), Anywhere in general.
 *       Notes:
 *************************************************************************/
void SetRightDrive(unsigned char value)
{

#ifdef DRIVE_DEBUG
    PrintToScreen("[drive.c] SetRightDrive(%d)\r", (int)value);
#endif

#ifdef RIGHT_DRIVE_INVERT
    SetPWM(RIGHT_DRIVE_MOTOR, 254 - value);
#else
    SetPWM(RIGHT_DRIVE_MOTOR, value);
#endif

}   /* void SetRightDrive(unsigned char value) */

/**************************************************************************
 *    Function: GetDriveHeading
 *     Purpose: Access the heading read by the drive train's gyro.
 *   Arguments: void
 *     Returns: A signed integer represeting the orientation, in degrees.
 * Called From: 
 *       Notes:
 *************************************************************************/
int GetDriveHeading(void)
{
    return (GetGyroAngle(DRIVE_GYRO) / 10);

}   /* int GetDriveHeading(void) */


/**************************************************************************
 *    Function: GetDriveHeading
 *     Purpose: Access the heading read by the drive train's gyro.
 *   Arguments: void
 *     Returns: An integer represeting the orientation, in degrees.
 *              Similar to GetDriveHeadinge, except it rolls over from 360
 *              to 0 (clockwise) and from 0 to 360 (counterclockwise).
 * Called From: 
 *       Notes:
 *************************************************************************/
int GetDriveHeadingWithRollover(void)
{
    int heading = GetDriveHeading();

    while (heading > 360) heading -= 360;
    while (heading < 0) heading += 360;

    return heading;

}   /* int GetDriveHeadingWithRollover(void) */


/**************************************************************************
 *    Function: ControlDrive
 *     Purpose: Controls the drive train via Operator Interface inputs.
 *   Arguments: void
 *     Returns: void
 * Called From: operator.c/OperatorControl()
 *       Notes: This is meant to be called iteratively in a loop;
 *              specifically only in operator.c/OperatorControl()
 *************************************************************************/
void ControlDrive(void)
{
    unsigned char left_joystick, right_joystick;

    left_joystick = GetOIAInput(LEFT_DRIVE_JOYSTICK, LEFT_DRIVE_JOYSTICK_AXIS);
    right_joystick = GetOIAInput(RIGHT_DRIVE_JOYSTICK, RIGHT_DRIVE_JOYSTICK_AXIS);

    /* TODO: Define Deadband settings: DRIVE_JOYSTICK_DEADBAND_LOW
                                       DRIVE_JOYSTICK_DEADBAND_HIGH
    */

    if (left_joystick > 112 && left_joystick < 142)     /* Left Joystick deadband */
        left_joystick = 127;
    if (right_joystick > 112 && right_joystick < 142)   /* Right Joystick deadband */
        right_joystick = 127;

    SetLeftDrive(left_joystick);
    SetRightDrive(right_joystick);

}   /* void ControlDrive(void) */
