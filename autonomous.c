/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: autnomous.c
 * Purpose: Autonomous routines.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 14 2007  Matt           Added sensor device startups/stops.    
 * May 16 2007  Matt           Added autonomous early alpha code. Needs
 *                             to be cleaned up, documented, etc. 
 *                             NOT TESTED!!                                    
 *
 *************************************************************************/

#include "gus.h"
#include "arm.h"
#include "claw.h"
#include "drive.h"
#include "elbow.h"
#include "elevator.h"

#define AUTO_ENABLESCARYSTUFF       /* uncomment this to enable autonomous testing. */

#define AUTO_SELECT_SWITCH2 12
#define AUTO_SELECT_SWITCH3 13

#define AUTO_SELECT_SWITCH0 5
#define ENCODER_TARGET_DISTANCE 660 //620 //620 //600 //500 //600 //880 //440
#define ENCODER_TARGET_STARTARM 120 //140 //160 //200 //300 //200 //300 //440 //220
#define ENCODER_TARGET_OPENCLAW 625 //540 //560 //540

void Auto_DoScaryStuff(void);

/**************************************************************************
 *    Function: Autonomous
 *     Purpose: Runs autonomous mode.
 *   Arguments: void
 *     Returns: void
 * Called From: WPILib
 *       Notes: 
 *************************************************************************/
void Autonomous(void)
{
    int switch3 = GetDigitalInput(AUTO_SELECT_SWITCH3);

#ifdef AUTO_ENABLESCARYSTUFF
    PrintToScreen("[autonomous.c] Autonomous(): SELECT SWITCH 3 = %d\r", switch3);
    if (!switch3)
    {
        Auto_DoScaryStuff();
    }
#endif

}

/**************************************************************************
 *    Function: Auto_DoScaryStuff
 *     Purpose: Runs a testing auto mode.
 *   Arguments: void
 *     Returns: void
 * Called From: Autonomous
 *       Notes: 
 *************************************************************************/
void Auto_DoScaryStuff(void)
{
    int mode;
    int arm_target;
    long left_encoder = 0, right_encoder = 0;
    int elevator_done = 0;
    int step = 0;
    int atEnd = 0;
    unsigned long endTime;

    mode = GetDigitalInput(AUTO_SELECT_SWITCH2);
    arm_target = (mode) ? ARM_TARGET_LEFT : ARM_TARGET_RIGHT;

    StartGyro(ARM_GYRO);
    StartGyro(DRIVE_GYRO);
    StartAccelerometer(DRIVE_ACCELEROMETER_X);
    StartAccelerometer(DRIVE_ACCELEROMETER_Y);
    PresetEncoder(LEFT_DRIVE_ENCODER, 0);
    PresetEncoder(RIGHT_DRIVE_ENCODER, 0);
    StartEncoder(LEFT_DRIVE_ENCODER);
    StartEncoder(RIGHT_DRIVE_ENCODER);

    CloseClaw();
    ElbowUp();

    while (1)
    {
        left_encoder = GetEncoder(LEFT_DRIVE_ENCODER);
        right_encoder = GetEncoder(RIGHT_DRIVE_ENCODER);

        if (!atEnd)
        {
            elevator_done = (ElevatorPID(ELEVATOR_TARGET_MIDDLE) == ELEVATOR_PID_ONTARGET);

            /* move the arm towards its target position after we've driven a specified distance. */
            if (left_encoder >= ENCODER_TARGET_STARTARM && right_encoder >= ENCODER_TARGET_STARTARM)
            {
                ArmPID(arm_target);
            }
        }
        else
        {
            SetElevatorMotor(115);      /* prevent backdriving */
            if ((GetMsClock() - endTime) > 1500)
                CloseClaw();
            //CloseClaw();                /* close claw .. */
            ArmPID(ARM_TARGET_CENTER);  /* move arm back to center */
        }

        /* driving */
        if (left_encoder <= ENCODER_TARGET_DISTANCE && right_encoder <= ENCODER_TARGET_DISTANCE)
        {
            SetLeftDrive(210);
            SetRightDrive(210);
        }
        else
        {
            SetLeftDrive(127);
            SetRightDrive(127);
        }

        if (left_encoder >= ENCODER_TARGET_OPENCLAW && right_encoder >= ENCODER_TARGET_OPENCLAW && !atEnd)
        {
            OpenClaw();
            atEnd = 1;
            endTime = GetMsClock();
        }
    }

    StopEncoder(RIGHT_DRIVE_ENCODER);
    StopEncoder(LEFT_DRIVE_ENCODER);
    PresetEncoder(RIGHT_DRIVE_ENCODER, 0);
    PresetEncoder(LEFT_DRIVE_ENCODER, 0);
    StopAccelerometer(DRIVE_ACCELEROMETER_Y);
    StopAccelerometer(DRIVE_ACCELEROMETER_X);
    StopGyro(DRIVE_GYRO);
    StopGyro(ARM_GYRO);
}



void Auto_DoScaryStuffX(void)
{
    /* TODO:
        Document. Test. Clean. Perfect.
    */

    int mode;
    int step = 0;
    long left_encoder = 0, right_encoder = 0; 
    int arm_target;
    int elevator_done = 0;

    mode = //GetDigitalInput(AUTO_SELECT_SWITCH0);
    mode = GetDigitalInput(AUTO_SELECT_SWITCH2);
    arm_target = (mode) ? ARM_TARGET_LEFT : ARM_TARGET_RIGHT;


    PrintToScreen("[autonomous.c] DoScaryStuff: MODE = %d\r", mode);

    StartGyro(ARM_GYRO);
    StartGyro(DRIVE_GYRO);
    StartAccelerometer(DRIVE_ACCELEROMETER_X);
    StartAccelerometer(DRIVE_ACCELEROMETER_Y);
    PresetEncoder(LEFT_DRIVE_ENCODER, 0);
    PresetEncoder(RIGHT_DRIVE_ENCODER, 0);
    StartEncoder(LEFT_DRIVE_ENCODER);
    StartEncoder(RIGHT_DRIVE_ENCODER);


    CloseClaw();
    ElbowUp();

    while (1)
    {
        left_encoder = GetEncoder(LEFT_DRIVE_ENCODER);
        right_encoder = GetEncoder(RIGHT_DRIVE_ENCODER);

        /* move the elevator up until it has reached the target position. */
       // if (!elevator_done)
        if (step < 2)
            elevator_done = (ElevatorPID(ELEVATOR_TARGET_MIDDLE) == ELEVATOR_PID_ONTARGET);
        else
            SetElevatorMotor(115);      /* prevent backdriving */

        /* move the arm towards its target position after we've driven a specified distance. */
        if (left_encoder >= ENCODER_TARGET_STARTARM || right_encoder >= ENCODER_TARGET_STARTARM)
        {
            ArmPID(arm_target);
        }

        if (step == 0)
        {
            PrintToScreen("[autonomous.c] DoScaryStuff @ Step 0\r");

            /* drive specified distance. */
            if (left_encoder <= ENCODER_TARGET_DISTANCE && right_encoder <= ENCODER_TARGET_DISTANCE)
            {
                int left_error = ENCODER_TARGET_DISTANCE - left_encoder;
                int right_error = ENCODER_TARGET_DISTANCE - right_encoder;

                int left_output = left_error * 5 / 10;
                int right_output = right_output * 5 / 10;

                /** PROPORTIONAL SPEED. MAY WANT TO CHANGE THIS?  TESTING NEEDED. **/

                if (left_output > 127)
                    left_output = 127;
                else if (right_output < -127)
                    right_output = -127;
                if (right_output > 127)
                    right_output = 127;
                else if (right_output < -127)
                    right_output = -127;
                
                left_output += 127;
                right_output += 127;
            
                SetLeftDrive((unsigned char)left_output);
                SetRightDrive((unsigned char)right_output);
            }
            else
            {
                /* drove far enough, stop & advance to the next step. */
                SetLeftDrive(127);
                SetRightDrive(127);
                step++;
            }
        }
        else if (step == 1)
        {
            PrintToScreen("[autonomous.c] DoScaryStuff at Step 1\r");
            /* open the claw.. move to the next step. */
            SetArmMotor(127);
            Wait(2000);     /* 2 second delay */
            OpenClaw();
            Wait(2000); /* 2 second delay */
            step++;
        }
        else if (step == 2)
        {
            CloseClaw();
            Wait(1000);
            PrintToScreen("[autonomous.c] DoScaryStuff Done\r");
            /* for now, we're done. */
            ArmPID(ARM_TARGET_CENTER);
            //ElevatorPID(ELEVATOR_TARGET_BOTTOM);
        }
    }

    StopEncoder(RIGHT_DRIVE_ENCODER);
    StopEncoder(LEFT_DRIVE_ENCODER);
    PresetEncoder(RIGHT_DRIVE_ENCODER, 0);
    PresetEncoder(LEFT_DRIVE_ENCODER, 0);
    StopAccelerometer(DRIVE_ACCELEROMETER_Y);
    StopAccelerometer(DRIVE_ACCELEROMETER_X);
    StopGyro(DRIVE_GYRO);
    StopGyro(ARM_GYRO);

}   /* void Autonomous(void) */
