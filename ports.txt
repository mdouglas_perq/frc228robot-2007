/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: ports.txt
 * Purpose: Listing of the port usage of the RC and OI.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Who            Comments
 * -----------------------------------------------------------------------
 * May 16 2007  Matt           File created.
 *                                            
 *
 *************************************************************************/

RC:

pwm01 - Left Drive
pwm02 - Right Drive
pwm03 - Elevator
pwm04 - Arm 

relay1 - Claw
relay7 - Elbow
relay8 - Pressure Switch

digital1 - Left Drive Encoder
digital2 - Right Drive Encoder
digital18 - Pressure Switch

analog1 - Drive Gyro
analog2 - Drive Accelerometer X
analog3 - Drive Accelerometer Y
analog4 - Elevator IR Sensor
analog5 - Arm Gyro


OI:

Joystick 1:
  Y-Axis - Left Drive


Joystick 2:
  Y-Axis - Right Drive
  Top - Claw Button
  Trig - Elbow Button

Joystick 3:
  Y-Axis - Elevator Manual Control
  Aux1 - Elevator Bottom Button
  Aux2 - Elevator Low Button
  Trig - Elevator Middle Button
  Top - Elevator Top Button

Joystick 4:
   X-Axis - Arm Control Potentiometer
