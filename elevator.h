/*************************************************************************
 * GUS Robotics Team 228
 * www.team228.org
 *
 * 2007 FIRST Robotics Competition
 * Robot Control Software, Offseason version
 *
 *    File: elevator.h
 * Purpose: Elevator control settings/declarations.
 *
 *************************************************************************
 *
 * REVISION HISTORY:
 *
 * -----------------------------------------------------------------------
 * Date         Programmer     Comments
 * -----------------------------------------------------------------------
 * May 12 2007  Matt           File created.
 * May 14 2007  Matt           Settings/Function prototypes added.
 * May 16 2007  Matt           Added ELEVATOR_DEBUG definition.
 *                                            
 *
 *************************************************************************/

#ifndef _ELEVATOR_H_
#define _ELEVATOR_H_

#define ELEVATOR_DEBUG

/** TODO: Document settings. **/

#define ELEVATOR_MOTOR 3
/*#define ELEVATOR_MOTOR_INVERT*/

#define ELEVATOR_IR_SENSOR 4

#define ELEVATOR_JOYSTICK PORT_3
#define ELEVATOR_BUTTON_BOTTOM AUX1_SW
#define ELEVATOR_BUTTON_LOW AUX2_SW
#define ELEVATOR_BUTTON_MIDDLE TRIGGER_SW
#define ELEVATOR_BUTTON_TOP TOP_SW

#define ELEVATOR_POSITION_BOTTOM 0
#define ELEVATOR_POSITION_LOW 1
#define ELEVATOR_POSITION_MIDDLE 2
#define ELEVATOR_POSITION_TOP 3

#define ELEVATOR_TARGET_BOTTOM 480
#define ELEVATOR_TARGET_LOW 215
#define ELEVATOR_TARGET_MIDDLE 70
#define ELEVATOR_TARGET_TOP 57
#define ELEVATOR_ERROR_THRESHOLD 15

#define ELEVATOR_PID_RUNNING 0
#define ELEVATOR_PID_ONTARGET 1

#define ELEVATOR_PID_KP 6 / 10
#define ELEVATOR_PID_KI 0 / 10
#define ELEVATOR_PID_KD 0 / 10

void SetElevatorMotor(unsigned char value);
int ElevatorPID(int target);
void ControlElevator(void);

#endif  /* #ifndef _ELEVATOR_H_ */
